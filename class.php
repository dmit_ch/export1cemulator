<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Web\HttpClient;
use Bitrix\Main\IO\File;
use Bitrix\Main\Application;

/**
 * Class IntervolgaExchange1cEmulator
 * Класс эмулирует первые три шага протокола выгрузки из 1С
 * http://dev.1c-bitrix.ru/api_help/sale/algorithms/data_2_site.php
 */
class IntervolgaExchange1cEmulator extends CBitrixComponent
{
	const DEFAULT_EXCHANGE_TYPE = "catalog";
	const AUTH_STEP_PARAMS_CNT = 5;
	const INIT_STEP_PARAMS_CNT = 4;

	private $exchangeFileName = "1c_exchange.php";
	private $errors = array();
	private $displayParams = "";
	/**
	 * @var Bitrix\Main\HttpRequest
	 */
	private $curRequest;

	public function onPrepareComponentParams($params)
	{
		if ($params["EXCHANGE_FILE_NAME"]) {
			$this->exchangeFileName = $params["EXCHANGE_FILE_NAME"];
		}

		return $params;
	}

	function executeComponent()
	{
		global $USER;
		$this->curRequest = Application::getInstance()->getContext()->getRequest();

		if ($USER->IsAuthorized() && $USER->IsAdmin()) {
			$isBackToPrevStep = strlen($this->curRequest->getPost("PREV_STEP")) > 0;
			$isSendAuth = strlen($this->curRequest->getPost("SEND_AUTH")) > 0;
			$isSendInit = strlen($this->curRequest->getPost("SEND_INIT")) > 0;
			$isSendFile = strlen($this->curRequest->getPost("SEND_FILE")) > 0;

			if (!isset($_SESSION["STEP_NUM"])) {
				$_SESSION["STEP_NUM"] = 1;
			}
			if ($isBackToPrevStep) {
				$this->stepBack();
			}
			$this->arResult["CUR_STATE"]["CUR_STEP"] = $_SESSION["STEP_NUM"];

			if ($isSendAuth) {
				$url = $this->getBaseUrl();
				$this->sendAuth($url);
			} elseif ($isSendInit) {
				$url = $this->getBaseUrl();
				$this->sendInit($url);
			} elseif ($isSendFile) {
				$url = $this->getBaseUrl();
				$this->sendFile($url);
			}
		} else {
			$this->errors[] = "Доступ есть только для админинистраторов. Авторизуйтесь.";
			$this->arResult["ACCESS_DENIED"] = "Y";
		}

		$this->arResult["ERRORS"] = $this->errors;
		$this->arResult["DISPLAY_PARAMS"] = $this->displayParams;
		$this->includeComponentTemplate();
	}

	private function stepBack()
	{
		$_SESSION["STEP_NUM"]--;
		switch ($_SESSION["STEP_NUM"])
		{
			case 2:
				unset($_SESSION["FILE_PATH"]);
				unset($_SESSION["ZIP_PARAM"]);
				unset($_SESSION["FILE_LIMIT_PARAM"]);
				unset($_SESSION["VERSION_PARAM"]);
				break;
			case 1:
				unset($_SESSION["COOKIE_HEAD"]);
				unset($_SESSION["SESSION_ID_PARAM"]);
				unset($_SESSION["IMPORT_TYPE"]);
				break;
		}
	}

	/**
	 * @return string
	 */
	private function getBaseUrl()
	{
		$httpHost = Application::getInstance()->getContext()->getServer()->getHttpHost();
		$url = "http://" . $httpHost . "/bitrix/admin/" . $this->exchangeFileName;
		if (!isset($_SESSION["IMPORT_TYPE"])) {
			$_SESSION["IMPORT_TYPE"] = $this->curRequest->getPost("TYPE");
			if (empty($_SESSION["IMPORT_TYPE"])) {
				$_SESSION["IMPORT_TYPE"] = self::DEFAULT_EXCHANGE_TYPE;
			}
		}
		$this->arResult["CUR_STATE"]["IMPORT_TYPE"] = $_SESSION["IMPORT_TYPE"];
		$url .= "?type=" . $_SESSION["IMPORT_TYPE"];

		return $url;
	}

	/**
	 * @param $url
	 */
	private function sendAuth($url)
	{
		global $USER;
		$login = $USER->GetLogin();
		$password = $this->curRequest->getPost("ADMIN_PASS");
		$authParams = base64_encode($login . ":" . $password);

		$url .= "&mode=checkauth";
		$client = new HttpClient();
		$client->setHeader("Authorization", "Basic " . $authParams);

		$response = $client->post($url);
		$params = explode("\n", trim($response));
		if (count($params) > self::AUTH_STEP_PARAMS_CNT) {
			$this->errors[] = "Ошибка авторизации";
		} elseif ($params[0] == "failure") {
			$this->displayParams = $response;
			$this->errors[] = $params[1];
		} else {
			$this->displayParams = $response;
			$_SESSION["COOKIE_HEAD"] = $params[1] . "=" . $params[2];
			$_SESSION["SESSION_ID_PARAM"] = $params[3];
			$_SESSION["STEP_NUM"] = 2;
			$this->arResult["CUR_STATE"]["CUR_STEP"] = $_SESSION["STEP_NUM"];
			$this->arResult["CUR_STATE"]["COOKIE_HEAD"] = $_SESSION["COOKIE_HEAD"];
			$this->arResult["CUR_STATE"]["SESSION_ID_PARAM"] = $_SESSION["SESSION_ID_PARAM"];
		}
	}

	/**
	 * @param $url
	 */
	private function sendInit($url)
	{
		$url .= "&mode=init";
		if (!empty($_SESSION["SESSION_ID_PARAM"])) {
			$url .= "&" . $_SESSION["SESSION_ID_PARAM"];
		}
		if ($version1c = $this->curRequest->getPost("VERSION")) {
			$url .= "&version=" . $version1c;
		}

		$client = new HttpClient();
		$client->setHeader("Cookie", $_SESSION["COOKIE_HEAD"]);
		$response = $client->post($url);
		$params = explode("\n", trim($response));
		if (count($params) > self::INIT_STEP_PARAMS_CNT) {
			$this->errors[] = "Ошибка инициализации обмена";
		} elseif ($params[0] == "failure") {
			$this->displayParams = $response;
			$this->errors[] = $params[1];
		} else {
			$this->displayParams = $response;
			$_SESSION["ZIP_PARAM"] = $params[0];
			$_SESSION["FILE_LIMIT_PARAM"] = $params[1];
			if (isset($params[2])) {
				$_SESSION["SESSION_ID_PARAM"] = $params[2];
			}
			$_SESSION["VERSION_PARAM"] = $params[3];
			$_SESSION["STEP_NUM"] = 3;
			$this->arResult["CUR_STATE"]["CUR_STEP"] = $_SESSION["STEP_NUM"];
			$this->arResult["CUR_STATE"]["COOKIE_HEAD"] = $_SESSION["COOKIE_HEAD"];
			$this->arResult["CUR_STATE"]["ZIP_PARAM"] = $_SESSION["ZIP_PARAM"];
			$this->arResult["CUR_STATE"]["FILE_LIMIT_PARAM"] = $_SESSION["FILE_LIMIT_PARAM"];
			$this->arResult["CUR_STATE"]["SESSION_ID_PARAM"] = $_SESSION["SESSION_ID_PARAM"];
			$this->arResult["CUR_STATE"]["VERSION_PARAM"] = $_SESSION["VERSION_PARAM"];
		}
	}

	/**
	 * @param $url
	 * @throws \Bitrix\Main\IO\FileNotFoundException
	 */
	private function sendFile($url)
	{
		if ($_FILES["SENDED_FILE"]) {
			$fileParams = array_merge($_FILES["SENDED_FILE"], array("del" => "Y"));
			$fId = \CFile::SaveFile($fileParams, "/1cImportImFiles");
			$fileInfo = \CFile::GetFileArray($fId);
			if ($fileInfo["SRC"]) {
				$_SESSION["FILE_PATH"] = $fileInfo["SRC"];
			} else {
				$this->errors[] = "Файл не был сохранен";
			}
		}

		if (isset($_SESSION["FILE_PATH"])) {
			$file = new File(Application::getInstance()->getDocumentRoot() . $_SESSION["FILE_PATH"]);
			if ($file->isExists()) {
				$url .= "&mode=file";
				$url .= "&filename=" . $file->getName();
				if (!empty($_SESSION["SESSION_ID_PARAM"])) {
					$url .= "&" . $_SESSION["SESSION_ID_PARAM"];
				}

				$client = new HttpClient();
				$client->setHeader("Cookie", $_SESSION["COOKIE_HEAD"]);
				$response = $client->post($url, $file->getContents());
				$params = explode("\n", trim($response));
				if ($params[0] != "success") {
					if ($params[0] == "failure") {
						$this->displayParams = $response;
						$this->errors[] = $params[1];
					} else {
						$this->errors[] = "Ошибка отправки файла";
					}
				} else {
					$this->displayParams = $response;
					$this->arResult["CUR_STATE"]["CUR_STEP"] = $_SESSION["STEP_NUM"];
					$this->arResult["CUR_STATE"]["COOKIE_HEAD"] = $_SESSION["COOKIE_HEAD"];
					$this->arResult["CUR_STATE"]["SESSION_ID_PARAM"] = $_SESSION["SESSION_ID_PARAM"];
					$this->arResult["CUR_STATE"]["FILE_PATH"] = $_SESSION["FILE_PATH"];
				}
			} else {
				$this->errors[] = "Файл не найден";
			}
		} else {
			$this->errors[] = "Ошибка загрузки файла";
		}
	}
}