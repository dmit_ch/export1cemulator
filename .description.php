<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => "Эмулятор выгрузки из 1С",
	"DESCRIPTION" => "Эмулирует первые три шага протокола выгрузки из 1С",
	"CACHE_PATH" => "Y",
	"SORT" => 10,
	"PATH" => array(
		"ID" => "intervolga.ru",
		"NAME" => "Интерволга",
	),
);
?>