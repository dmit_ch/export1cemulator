<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if ($arResult["ACCESS_DENIED"] == "Y"):?>
	<?ShowError(current($arResult["ERRORS"]))?>
<?else:?>
	<?if (count($arResult["ERRORS"]) > 0):?>
		<?foreach ($arResult["ERRORS"] as $error):?>
			<?ShowError($error)?>
		<?endforeach;?>
	<?endif;?>

	<textarea readonly><?=$arResult["DISPLAY_PARAMS"]?></textarea><br/>
	<form method="post" action="" enctype="multipart/form-data" onkeypress="if(event.keyCode == 13) return false;">
		<?
		switch ($arResult["CUR_STATE"]["CUR_STEP"])
		{
			case 1:?>
				<select name="TYPE">
					<option value="catalog">Торговый каталог</option>
					<option value="sale">Заказы</option>
					<option value="reference">Highload-блок</option>
				</select>
				<input type="password" name="ADMIN_PASS" value="" placeholder="Password"/>
				<input type="submit" name="SEND_AUTH" value="AUTH"/><?
				break;
			case 2:?>
				<input type="text" name="VERSION" placeholder="1C Version"/>
				<input type="submit" name="SEND_INIT" value="INIT"/><?
				break;
			case 3:?>
				<input type="file" name="SENDED_FILE" value=""/><br/>
				<input type="submit" name="SEND_FILE" value="FILE"/><?
				break;
			default:
				ShowError("INVALID STEP ERROR");
		}
		?>
		<?if ($arResult["CUR_STATE"]["CUR_STEP"] > 1):?>
			<input type="submit" name="PREV_STEP" value="Назад"/>
		<?endif;?>
	</form>
<?endif;?>