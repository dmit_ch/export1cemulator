<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arComponentParameters = array(
	"PARAMETERS" => array(
		"EXCHANGE_FILE_NAME" => array(
			"PARENT" => "BASE",
			"NAME" => "Имя файла процедуры обмена",
			"TYPE" => "STRING",
			"DEFAULT" => "1c_exchange.php",
		),
	),
);